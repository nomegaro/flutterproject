import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_application/pages/home_page.dart';

class PasswordSetPage extends StatefulWidget {
  final User? user;

  const PasswordSetPage({Key? key, required this.user}) : super(key: key);

  @override
  _PasswordSetPageState createState() => _PasswordSetPageState();
}

class _PasswordSetPageState extends State<PasswordSetPage> {
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  bool _isLoading = false;

  Future<void> _setPassword(String password) async {
    setState(() {
      _isLoading = true;
    });

    try {
      final String email = '${widget.user?.phoneNumber}@example.com';

      await widget.user?.updateEmail(email);
      await widget.user?.updatePassword(password);

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("Нууц үг амжилттай тохируулагдлаа!"),
        ),
      );

      // Navigate to home page after successful password set
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => const HomePage()),
      );
    } on FirebaseAuthException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("Нууц үг тохируулахад алдаа гарлаа: ${e.message}"),
        ),
      );
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Нууц үг тохируулах"),
        backgroundColor: Colors.red.shade400,
        elevation: 0,
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  style: const TextStyle(
                    color: Colors.red,
                    fontSize: 18,
                  ),
                  decoration: InputDecoration(
                    labelText: "Нууц үг",
                    helperText: "Нууц үг оруулна уу",
                    prefixIcon: const Icon(Icons.lock, color: Colors.red),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.red.shade400,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.red.shade400,
                      ),
                    ),
                    labelStyle: const TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Нууц үг оруулна уу";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: _confirmPasswordController,
                  obscureText: true,
                  style: const TextStyle(
                    color: Colors.red,
                    fontSize: 18,
                  ),
                  decoration: InputDecoration(
                    labelText: "Нууц үг давтах",
                    helperText: "Нууц үг давтах оруулна уу",
                    prefixIcon: const Icon(Icons.lock, color: Colors.red),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.red.shade400,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.red.shade400,
                      ),
                    ),
                    labelStyle: const TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Нууц үг давтах оруулна уу";
                    }
                    if (value != _passwordController.text) {
                      return "Нууц үг зөрж байна";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 24),
                SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: _isLoading
                        ? null
                        : () {
                            if (_formKey.currentState!.validate()) {
                              _setPassword(_passwordController.text.trim());
                            }
                          },
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.red.shade400),
                      shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    child: _isLoading
                        ? const CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          )
                        : const Text(
                            "Нууц үг тохируулах",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
