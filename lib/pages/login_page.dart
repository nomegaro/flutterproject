import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application/components/my_button.dart';
import 'package:flutter_application/components/my_textfield.dart';
import 'package:flutter_application/pages/home_page.dart';
import 'package:flutter_application/pages/signup_page.dart';

class LoginPage extends StatelessWidget {
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  late final FirebaseAuth _auth = FirebaseAuth.instance;

  LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 10),
                const ImageIcon(
                  AssetImage("lib/images/food_delivery.png"),
                  color: Colors.red,
                  size: 230,
                ),
                const SizedBox(height: 0),
                Text(
                  'Тавтай морилно уу!',
                  style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 16,
                  ),
                ),
                const SizedBox(height: 25),
                MyTextField(
                  key: const Key('phoneNumber'),
                  controller: phoneNumberController,
                  hintText: 'Утасны дугаар',
                  obscureText: false,
                ),
                const SizedBox(height: 10),
                MyTextField(
                  key: const Key('password'),
                  controller: passwordController,
                  hintText: 'Нууц үг',
                  obscureText: true,
                ),
                const SizedBox(height: 25),
                MyButton(
                  label: 'Нэвтрэх',
                  onTap: () => signUserIn(context),
                ),
                const SizedBox(height: 10),
                const SizedBox(height: 130),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SignUpPage()),
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        'Бүртгэлтэй юу?',
                        style: TextStyle(
                            color: Color.fromARGB(255, 82, 82, 82),
                            fontSize: 16),
                      ),
                      SizedBox(width: 4),
                      Text(
                        'Бүртгүүлэх',
                        style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> signUserIn(BuildContext context) async {
    final phone = phoneNumberController.text.trim();
    final password = passwordController.text.trim();

    if (phone.isEmpty || password.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          key: Key('empty_fields_snackbar'),
          content: Text('Утасны дугаар ба нууц үгээ оруулна уу'),
        ),
      );
      return;
    }

    _showProgressDialog(context);

    try {
      final userCredential = await _auth.signInWithEmailAndPassword(
        email: '+976$phone@example.com',
        password: password,
      );
      final user = userCredential.user;

      if (user == null) {
        throw Exception('Хэрэглэгч олдсонгүй');
      }

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) => const HomePage()),
      );

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Амжилттай нэвтэрлээ'),
        ),
      );
    } on FirebaseAuthException catch (e) {
      String errorMessage;

      switch (e.code) {
        case 'user-not-found':
          errorMessage = 'Хэрэглэгч олдсонгүй';
          break;
        case 'wrong-password':
          errorMessage = 'Нууц үг эсвэл дугаар буруу байна';
          break;
        default:
          errorMessage = 'Алдаа гарлаа';
      }

      _showErrorSnackbar(context, errorMessage);
    } catch (e) {
      _showErrorSnackbar(context, 'Алдаа гарлаа');
    } finally {
      Navigator.of(context).pop();
    }
  }

  void _showProgressDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) => const Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
        ),
      ),
    );
  }

  void _showErrorSnackbar(BuildContext context, String errorMessage) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        key: const Key('firebase_auth_error_snackbar'),
        content: Text(errorMessage),
      ),
    );
  }
}
